package gui;

import gm.Cell;
import gm.CellFill;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

public abstract class Entity extends JComponent {
	
	private Image image;
	private Cell cell;
		
	public Entity(Image image, Window window, Cell cell) throws IOException {
		this.cell = cell;
		this.image = image;
		Dimension size = new Dimension(image.getWidth(null), image.getHeight(null));
	    setPreferredSize(size);
	    setMinimumSize(size);
	    setMaximumSize(size);
	    setSize(size);
	    setLayout(null);
	    int y = Math.abs(window.getBoard().getNumberOfCellsY() - cell.getY() - 1);
	    setBounds(cell.getX()*(window.getWidth()/window.getBoard().getNumberOfCellsX()), 
	    		  y*(window.getHeight()/window.getBoard().getNumberOfCellsY()), 
	    		  window.getWidth()/window.getBoard().getNumberOfCellsX(), 
	    		  window.getHeight()/window.getBoard().getNumberOfCellsY());
	}
	
	public static Image renderImage(File file, Window window) throws IOException {
		return ImageIO.read(file).getScaledInstance(window.getWidth()/window.getBoard().getNumberOfCellsX(),
				  window.getHeight()/window.getBoard().getNumberOfCellsY(),
				  Image.SCALE_SMOOTH);
	}
	
	@Override
	public void paintComponent(Graphics g) { 
		g.drawImage(image, 0, 0, null);
	}

	public CellFill getFill() {
		return cell.getFill();
	}


}
