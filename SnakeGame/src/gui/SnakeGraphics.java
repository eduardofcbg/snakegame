package gui;

import gm.Cell;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class SnakeGraphics extends Entity {

	private String lookAndFeel;
	
	public SnakeGraphics(Image image, Window window, Cell cell) throws IOException {
		super(image, window, cell);
	}

	public String getLookAndFeel() {
		return lookAndFeel;
	}

	public void setLookAndFeel(String lookAndFeel) {
		this.lookAndFeel = lookAndFeel;
	}
	
	

}
