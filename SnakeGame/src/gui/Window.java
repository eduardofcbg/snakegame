package gui;

import gm.Board;
import gm.Cell;
import gm.CellFill;
import gm.InvalidLocationException;
import gm.Snake;

import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Window extends JFrame implements Observer {
	
	private volatile boolean gameOver; //the window also should awere of game's state
	
	private Board board;
	private int size;
	
	private Snake selectedSnake;
	private Cell selectedCell;
		
	private LinkedList<Entity> graphics; //keeping track of what is drawn in the board, for more efficient repaint
	
	private HashMap<CellFill, List<Image>> renderedImages; //save rendered images
	private HashMap<String, Image> graphicsMapping; //mapping snake graphics to snake
	
	public Window(String title, final int size, Board board) throws IOException, HeadlessException {
		this.board = board;
		this.size = size;
		this.gameOver = false;
		this.graphics = new LinkedList<>();
		this.renderedImages = new HashMap<>();
		this.graphicsMapping = new HashMap<>();
		setLayout(null);
        setSize(size, size);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
        setTitle(title);
		board.addObserver(this);
		
		renderGraphics();
		mapGraphics();
        add(new Grid(this, size, board.getNumberOfCellsY()));
        drawBoard();
        new BackroundCleaner().start(); //seperate thread that removes ui elements that are not visible / in use
        
        //this mouse listener only listens for clicks made over obstacles
        addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent e) {}
			@Override
			public void mousePressed(MouseEvent e) {
				handleClickEvent(e);
			}
			@Override
			public void mouseExited(MouseEvent e) {}
			@Override
			public void mouseEntered(MouseEvent e) {}
			@Override
			public void mouseClicked(MouseEvent e) {}
		});        
	}
	
	private void renderGraphics() throws IOException {
		List<Image> snakeImages = new ArrayList<>();
		for(int i = 1; i < 11; i++) { //TODO fix this!
			snakeImages.add(Entity.renderImage(new File("assets/snake" + i + ".png"), this));
		}
		renderedImages.put(CellFill.SNAKE, snakeImages);
		renderedImages.put(CellFill.OBSTACLE, Arrays.asList(Entity.renderImage(new File("assets/brick.png"), this)));
		renderedImages.put(CellFill.FOOD, Arrays.asList(Entity.renderImage(new File("assets/food.gif"), this)));
		renderedImages.put(CellFill.BOOST, Arrays.asList(Entity.renderImage(new File("assets/boost.png"), this)));
		renderedImages.put(CellFill.JUMP, Arrays.asList(Entity.renderImage(new File("assets/jump.png"), this)));
	}
	
	private Image getImage(Cell cell) {
		if (cell.hasObstable())
			return renderedImages.get(CellFill.OBSTACLE).get(0);
		if (cell.hasFood())
			return renderedImages.get(CellFill.FOOD).get(0);
		if (cell.hasBoost())
			return renderedImages.get(CellFill.BOOST).get(0);
		if (cell.hasJump())
			return renderedImages.get(CellFill.JUMP).get(0);
		if (cell.hasSnake())
			return graphicsMapping.get(cell.getLookAndFeel());
		return null;
	}
	
	private void mapGraphics() {
		//TODO make sude different snake don't end up with the same image!!
		//is only called whe the game starts. This way the client knows which snake have which graphics
		List<Image> snakeImages = renderedImages.get(CellFill.SNAKE);
		Random rand = new Random();
		for(Cell cell : board.getCells()) {
			if (cell.hasSnake()) {
				int r = rand.nextInt(snakeImages.size());
				graphicsMapping.put(cell.getLookAndFeel(), snakeImages.get(r));
			}
		}
	}
		
	private void drawBoard() throws IOException {
		drawBoard(true);
	}
	
	private void drawBoard(boolean drawStatic) throws IOException { //populates references to UI graphics		
		for(Cell cell : board.getCells()) {							//for more effecient removal on update
			if (cell.hasObstable() && drawStatic)				    //only draws graphics that don't change 
				add(new ObstacleGraphics(getImage(cell), this, cell));				//depending on the flag passed
			if (cell.hasSnake()) {
				SnakeGraphics snakeCell = new SnakeGraphics(getImage(cell), this, cell);
				add(snakeCell);
				graphics.add(snakeCell);
			}
			if (cell.hasBoost()) {
				BoostGraphics boostCell = new BoostGraphics(getImage(cell), this, cell);
				add(boostCell);
				graphics.add(boostCell);
			}
			if (cell.hasFood()) {
				FoodGraphics foodCell = new FoodGraphics(getImage(cell), this, cell);
				add(foodCell);
				graphics.add(foodCell);
			}
			if (cell.hasJump()) {
				JumpGraphics jumpCell = new JumpGraphics(getImage(cell), this, cell);
				add(jumpCell);
				graphics.add(jumpCell);
			}
		}
	}
	
	public void handleClickEvent(MouseEvent e) {
		Cell cell = null;
		Snake snake = null;
		try {
			cell = board.mapCell(e.getX()/(size/getBoard().getNumberOfCellsX()), Math.abs(e.getY() - size)/(size/getBoard().getNumberOfCellsY()));
		} catch (InvalidLocationException e1) {}
		if (!cell.hasSnake()) selectedCell = cell;
		snake = cell.getSnake(board); //replace when making server //TODO
		if (snake != null) selectedSnake = snake; //in case the selected cell doesn't belong to a snake,
												  //the user is selecting a destiny for the selected snake
//		System.out.println(selectedSnake + " -> " + selectedCell);	
		if (selectedCell != null && selectedSnake != null) orderMovement();
	}
		
	private void orderMovement() {
		//replace when making server //TODO
		board.setSelected(selectedCell, selectedSnake);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 != null && !gameOver) { //if case the game is over, it's passed the snake that won
			if (arg1.getClass().equals(Snake.class)) {
				JOptionPane.showMessageDialog(this, "Snake " + ((Snake) arg1).getKey() + " has won!");
				endGame();
			}
		} else {
		    try {
		    	synchronized (this) {
			    	for(Entity graphic : graphics) {
			    		graphic.setVisible(false);
			    	}
				}
		    	synchronized (this) {
					drawBoard(false); //draw board, only snakes and power ups (obstacles are static)
					repaint();
				}
			} catch (IOException e) {}
		}
	}
	
	private class BackroundCleaner extends Thread {
		@Override
		public void run() {
			while(true) {
				try { Thread.sleep(1000); } catch (InterruptedException e) {}
		    	for(Entity graphic : graphics) {
		    		if (!graphic.isVisible()) remove(graphic);
		    	}
		    	graphics.removeIf(graphic -> !graphic.isVisible());
			}
		}
	}

	private void endGame() {
		gameOver = true;
	}	
	
	public Board getBoard() {
		return board;
	}
	
	public void init() {
		setVisible(true);
	}

	
	
}
