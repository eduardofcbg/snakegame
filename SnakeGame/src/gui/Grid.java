package gui;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import gm.Cell;

public class Grid extends JPanel {

    private int size; //side in pixels
    private int side; //number of cell
        
    private Window window; //reference to main window. Window will handle all the click events
    
    public Grid(Window window, int size, int side) {
        this.window = window;
        this.size = size;
        this.side = side;
        setSize(size*size, size*size); //grid's size only needs to be significantly bigger than the actual window size
        							   //this doesn't seem to impact performance
        
        //this mouse listener only works for clicks made directly on the grid (not over obstacles)
        addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent e) {}
			@Override
			public void mousePressed(MouseEvent e) {
				getWindow().handleClickEvent(e);
			}
			@Override
			public void mouseExited(MouseEvent e) {}
			@Override
			public void mouseEntered(MouseEvent e) {}
			@Override
			public void mouseClicked(MouseEvent e) {}
		});        
    }
    
    public int[][] mapCell(Cell cell) { //returns two coordinates that represent the dimensions of the cell
    	int[][] coordinates = new int[2][2];
    	coordinates[0][0] = cell.getX()*(size/side); 
    	coordinates[1][0] = cell.getY()*(size/side);
    	coordinates[0][1] = (cell.getX()+1)*(size/side);
    	coordinates[1][1] = (cell.getY()+1)*(size/side);
    	return coordinates;
    }
    
    public void paint(Graphics g) {
        int i;
        // draw the rows
        int cellSize = size / side;
        for (i = 0; i < side; i++)
            g.drawLine(0, i * cellSize, size, i * cellSize);
        // draw the columns
        for (i = 0; i < side; i++)
            g.drawLine(i * cellSize, 0, i * cellSize, size);
    }

    private Window getWindow() {
		return window;
	}

    

}
