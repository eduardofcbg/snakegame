package tests;

import gm.Cell;
import gm.Direction;

public class Test {

	public static void main(String[] args) {

		Cell cell1 = new Cell(1, 1);
		Cell cell2 = new Cell(5, 2);
		
		System.out.println(Direction.generateDirection(cell1, cell2));
	}

}
