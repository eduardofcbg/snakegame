package gm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Random;

public class Board extends Observable implements Runnable {

	public static final int MINIMUM_PLAYERS = 2;
	public static final int MAXIMUM_PLAYERS = 4;
	
	private static final double FRAME_RATE = 0.001;
	private static final int MULTIPLIER = 2; //how many times the gui updates faster than the snake movements
	public static final int SLEEPTIME = (int) (1 / (FRAME_RATE * MULTIPLIER));
	public static final int SNAKE_SLEEPTIME = (int) (1 / FRAME_RATE);
	
	public static final int FOOD_SIZE = 3;
	public static final int JUMP_SIZE = 3;
	public static final int BOOST_SIZE = 3;
	
	private ArrayList<Cell> cells;
	private ArrayList<Snake> snakes;
	private List<String> keys;
	
	private int numberCells;	
	private int numberObstacles; 
	private int numberOfPowerUps;
				
	public Board(int numberCells, List<String> keys) throws IllegalArgumentException { 
		if (keys.size() > 4) throw new IllegalArgumentException();
		this.numberCells = numberCells;
		this.keys = keys;
		this.numberObstacles = (int) (Math.random()*(numberCells*numberCells/20-numberCells*numberCells/30) + numberCells*numberCells/30);
		this.numberOfPowerUps = (numberObstacles / 2);
		this.cells = Cell.allCells(numberCells, numberCells);
		this.snakes = new ArrayList<Snake>();
		populate();
		int i = 0;
		for(String key : keys) {
			snakes.add(new Snake(key, 5, Direction.getDirection(i), this));
			i++;
		}
//		snakes.add(new Snake("Rosario", 5, Direction.RIGHT, this));
//		snakes.add(new Snake("Figueira", 5, Direction.LEFT, this));
//		snakes.add(new Snake("Figueira", 5, Direction.LEFT, this));
//		snakes.add(new Snake("Nogueira", 5, Direction.DOWN, this));
//		snakes.add(new Snake("Silvas", 5, Direction.UP, this));
        start();
	}
	
	private void populate() {
		//this method is called before the snakes are placed on the board7.
		//populate obstacles
		for(int i = 0; i < numberObstacles; i++) {
			Cell cell = null;
			while(cell == null) {
				int randomCell = (int) (Math.random()*cells.size());
				cell = cells.get(randomCell);
				if (!cell.isEmpty()) cell = null;
			}	
			cell.placeObstable();
		}
		//populate powerups
		for(int i = 0; i < numberOfPowerUps; i++) {
			int r = new Random().nextInt(3);
			Cell cell = null;
			while(cell == null) {
				int randomCell = (int) (Math.random()*cells.size());
				cell = cells.get(randomCell);
				if (!cell.isEmpty()) cell = null;
			}
			if (r == 0) cell.placeBoost(); 
			if (r == 1) cell.placeFood();
			if (r == 2) cell.placeJump();
		}
	}
	
	private void start() {
		for(Snake snake : snakes)
			snake.start();
	}
	
	public void isStuck(Snake stuck) { 				 //whenever a snake is stuck, we check if only one is still in game
		snakes.removeIf( snake -> !snake.inGame() ); //if that's the case, game is over
		if (snakes.size() == 1) gameOver(snakes.get(0));
	}
	
	public void gameOver(Snake winner) {
		for(Snake snake : snakes)
			snake.interrupt();
		setChanged();
		notifyObservers(winner);
	}

    public Cell mapCell(int x, int y) throws InvalidLocationException { //maps the coordinate of a cell to 
        for(Cell cell : cells) {										//an instance of a cell in the board
            if (cell.getX() == x && cell.getY() == y)
                return cell;
        }
        throw new InvalidLocationException(x, y);
    }

	@Override
	public void run() {
		synchronized (this) {
			while(!gameWasUpdated()) { //if all snakes were updated, the UI will update
				try {
					wait();
				} catch (InterruptedException e) {}
				updateUI();
				setSnakesUpdated();
			}
		}
	}
	
	private boolean gameWasUpdated()  { //checks if all snakes where updated
		int counter = 0;
		for(Snake snake : snakes) {
			if (snake.toUpdate())
				counter++;
		}
		if (counter == snakes.size()) 
			return true;
		return false;
	}
	
	public synchronized void notifyUpdated() { //after every snake's move, this method is called
		notify();
	}
	
	private void setSnakesUpdated() { //set the state of the snakes has the same has the UI
		for(Snake snake : snakes) {
			snake.setUpdated(false);
		}
	}
	
	public List<Cell> getCells() {
		return cells;
	}
	
	public int getNumberOfCellsX() {
		return numberCells;
	}
	
	public int getNumberOfCellsY() {
		return numberCells;
	}
	
	public List<String> getKeys() {
		return keys;
	}
	
	public void updateUI()  {
		setChanged();
		notifyObservers();
	}
			
	public ArrayList<Snake> getSnakes() {
		return snakes;
	}
	
	public Snake getSnake(String key) {
		for(Snake snake : snakes) {
			if (snake.getKey().equals(key)) return snake;
		}
		return null;
	}
	
	public void setSelected(Cell cell, Snake snake) {
		snake.setDestiny(cell);
	}

//	private Direction changeToDesiredPath(Snake selectedSnake, Cell selectedCell) {
//		return Direction.generateDirection(selectedSnake.getCells().getLast(), selectedCell);
//	}

	
}
