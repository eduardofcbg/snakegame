package gm;

public class InvalidLocationException extends Exception {
	
    public InvalidLocationException(int x, int y) {
        super("(" + x + ", " + y + ") is an invalid location in board");
    }
        
    public InvalidLocationException() {
    	super();
    }
        
    

}
