package gm;

public class UnitVector extends Cell { //represents the direction of the snake

	public UnitVector(int x, int y) {
		super(x, y);
		if (x > 1 || y > 1) throw new IllegalArgumentException();
	}

}
