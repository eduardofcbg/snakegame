package gm;

import java.io.Serializable;
import java.util.*;

public class Cell implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int x;
	private int y;
	
	private CellFill fill;
	private String lookAndFeel;
	
	private LinkedList<Snake> snakes;
		
	public Cell(int x, int y, String lookAndFeel) {
		this.x = x;
		this.y = y;
		this.fill = CellFill.NONE;
		this.setLookAndFeel(lookAndFeel);
	}
	
	public Cell(int x, int y) {
		this.x = x;
		this.y = y;
		this.fill = CellFill.NONE;
	}
			
	public static ArrayList<Cell> allCells(int numberCellsX, int numberCellsY) {
		ArrayList<Cell> cells = new ArrayList<>();
		for(int x = 0; x < numberCellsX; x++) {
			for(int y = 0; y < numberCellsY; y++) {
				cells.add(new Cell(x, y));
			}
		}
		return cells;
	}
	
	public static List<Cell> near(Cell cell, Board board) throws InvalidLocationException { //returns list o cells around a cell
		List<Cell> cells = new LinkedList<>();
		cells.add(board.mapCell(cell.getX(), cell.getY() + 1));
		cells.add(board.mapCell(cell.getX() + 1, cell.getY()));
		cells.add(board.mapCell(cell.getX(), cell.getY() - 1));
		cells.add(board.mapCell(cell.getX() - 1, cell.getY()));
		return cells;
	}

	public static LinkedList<Cell> defaultSnakeStart(Snake snake, Board board) throws InvalidLocationException {
		Random rand = new Random();
		int randomY = rand.nextInt(board.getNumberOfCellsY() - 1);
		int randomX = rand.nextInt(board.getNumberOfCellsX() - 1);
	
		UnitVector vector = Direction.getDirection(snake.getFinalPosition());
		LinkedList<Cell> cells = new LinkedList<Cell>();
		
		//addds first cell to snake (tail)
		if (snake.getFinalPosition().equals(Direction.RIGHT))
			cells.add(board.mapCell(0, randomY));
		if (snake.getFinalPosition().equals(Direction.LEFT))
			cells.add(board.mapCell(board.getNumberOfCellsX() - 1, randomY));
		if (snake.getFinalPosition().equals(Direction.DOWN))
			cells.add(board.mapCell(randomX, board.getNumberOfCellsY() - 2));
		if (snake.getFinalPosition().equals(Direction.UP))
			cells.add(board.mapCell(randomX, 0));
		
		Cell tail = cells.getFirst();
		Cell cell = board.mapCell(tail.getX() + vector.getX(), tail.getY() + vector.getY());
		try {
			cells.add(cell);
			tail.getIn(false, snake);
			cell.getIn(false, snake);
			for(int i = 0; i < snake.getSize() - 2; i++) {
				cell = board.mapCell(cell.getX() + vector.getX(), cell.getY() + vector.getY());
				cells.add(cell);
				cell.getIn(false, snake);
			}
		} catch (InterruptedException e) {}
		return cells;
	}
	
	public void getIn(boolean check, Snake snake) throws InterruptedException {
		getIn(check, true, snake);
	}
	
	public synchronized void getIn(boolean check, boolean changeState, Snake snake) throws InterruptedException {
		while (!isAvaiable() && check) {
			System.out.println("Cell not avaiable! waiting!");
			wait();
			System.out.println("Cell is now avaiable! moving in!");
		}
		if (changeState)
			placeSnake();
		setLookAndFeel(Integer.toString(snake.hashCode()));
	}
	
	public synchronized void getOut() {
		fill = CellFill.NONE;
		setLookAndFeel(null);
		notify();
	}
		
	public boolean isDestiny(Snake snake) {
		try {
			if (snake.getDestiny().equals(this)) return true;
		} catch(NullPointerException e){} //throws exception when snake doesn't have a set destiny
		return false;
	}
	
	public Snake getSnake(Board board) {
		try {
			for(Snake snake : board.getSnakes()) {
				if (snake.getCells().contains(this))
					return snake;
			}
		} catch(NullPointerException e) {}
		return null;
	}
	
	public UnitVector normalize() {
		if (getLengthX() != 0)
			setX(x/getLengthX());
		if (getLengthY() != 0)
			setY(y/getLengthY());
		return new UnitVector(x, y);
	}
	
	private int getLengthX() {
		return Math.abs(x);
	}
	
	private int getLengthY() {
		return Math.abs(y);
	}
	
	public void placeSnake() {
		fill = CellFill.SNAKE;
	}

	public void placeObstable() {
		fill = CellFill.OBSTACLE;
	}
	
	public void placeBoost() {
		fill = CellFill.BOOST;
	}
	
	public void placeFood() {
		fill = CellFill.FOOD;
	}
	
	public void placeJump() {
		fill = CellFill.JUMP;
	}
	
	public boolean hasJump() {
		if (fill.equals(CellFill.JUMP)) return true;
		return false;
	}
	
	public boolean hasObstable() {
		if (fill.equals(CellFill.OBSTACLE)) return true;
		return false;
	}
	
	public synchronized boolean hasSnake(){
		if (fill.equals(CellFill.SNAKE)) return true;
		return false;
	}
	
	public synchronized boolean hasBoost() {
		if (fill.equals(CellFill.BOOST)) return true;
		return false;
	}
	
	public synchronized boolean hasFood() {
		if (fill.equals(CellFill.FOOD)) return true;
		return false;
	}
		
	public synchronized boolean isAvaiable() {
		return (isEmpty() || hasFood() || hasBoost() || hasJump());
	}
	
	public boolean isEmpty() {
		if (fill.equals(CellFill.NONE)) return true;
		return false;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public CellFill getFill() {
		return fill;
	}
		
	public String getLookAndFeel() {
		return lookAndFeel;
	}

	public void setLookAndFeel(String lookAndFeel) {
		this.lookAndFeel = lookAndFeel;
	}
	
	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return "x= " + x + ", y= " + y + ", Fill= " + fill;
	}
	
	
	
}
