package gm;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Snake extends Thread {

	public static int numberSnakes = 0;
	private boolean inGame;
		
	private String key;
	private LinkedList<Cell> cells;
	private Board board;
	private volatile Direction direction; //current snake's moving direction

	private Direction finalPosition; //final position on the board for the snake
	private Cell destiny;	
	
	public int size; //in cells
	
	private int foodCounter; //to keep track of the number of cells added for boost
	private int boostCounter;
	private boolean boostState;
	private boolean jumpState;
	
	private volatile boolean pushData; //flag meaning that the snake just moved and the GUI must be updated
										
	public Snake(String key, int size, Direction direction, Board board) {
        numberSnakes++;
		this.key = key;
		this.board = board;
		this.direction = direction;
		this.finalPosition = direction;
        this.size = size;
        this.foodCounter = 0;
        this.boostCounter = 0;
        this.boostState = false;
        this.jumpState = false;
        this.inGame = true;
        this.pushData = false;
		try {
			this.cells = Cell.defaultSnakeStart(this, board);
		} catch (InvalidLocationException e) {}
	}
		
	private List<Cell> getCellsNear() { //returns an array of possible next snake's viable positions
		List<Cell> cellsNear = new LinkedList<>();
		List<Cell> available = new LinkedList<>();
		Cell head = cells.getLast();
		try {
			cellsNear = Cell.near(head, board);
		} catch(InvalidLocationException e) {} //exception is thrown when snake is in the edge of the board
		for(Cell cell : cellsNear) {
			if (!cell.hasObstable() && !this.cells.contains(cell))
				available.add(cell);
		}
		return available;	
	}
	
	private Cell getDestination() throws StuckException {  //this method returns the next cell the snake should move to
		//if there is a set destiny for the snake, a direction for the snake will be calculated.
		//NOTE: this doesn't garantee that the snake will actually reach the set destiny.
		try {
			if (destiny != null) setDirection(Direction.generateDirection(cells.getLast(), destiny));
		} catch(InvalidDirection e) {System.out.println("a diagonal was choosen");}
			
		List<Cell> cellsNear = getCellsNear();
		if (cellsNear.size() == 0) throw new StuckException(); //snake is forever stuck
		Cell head = cells.getLast();
		UnitVector vector = Direction.getDirection(direction);
		for(Cell cell : cellsNear) {
			if (cell.isDestiny(this)) { //if snake has reached the selected cell, the destiny is removed and
				setDestiny(null);	//the direction will get back to default
				setDirection(finalPosition); //
				return cell; //if a cell is selected, snake will move there
			}
			try {
				if (cell.equals(board.mapCell(head.getX() + vector.getX(), head.getY() + vector.getY())))
					return cell; //if the cell matches the snake destination, it will move there
			} catch (InvalidLocationException e) {} //exception is thrown when cell is outside the board
		}
		int r = new Random().nextInt(cellsNear.size()); //in case none of the above, a random destination will be choosen
		return cellsNear.get(r);
	}
	
	public void move() throws InvalidLocationException, StuckException {
		//default move method is called without arguments,
		//but a flag can be passed, indicating whether the snake checks for cell availabilty or not
		//in case the snake is on jump mode, it does not check
		if (jumpState)
			move(false);
		else
			move(true);
	}
		
	public void move(boolean checkAvailable) throws InvalidLocationException, StuckException {
		//if snake has won, throws InvalidLocationException
		if (hasWon()) throw new InvalidLocationException();
		
		Cell nextCell = getDestination(); //based on the snake wants to go, a next cell is generated.
		
		try {
			nextCell.getIn(checkAvailable, false, this); //method blocks in case that cell is not available
		} catch (InterruptedException e) {}
		
		if (nextCell.hasFood()) {
			foodCounter += Board.FOOD_SIZE;
			System.out.println("yum...");
		}
		if (nextCell.hasBoost()) {
			boostState = true;
			System.out.println("nice boost");
		}
		if (nextCell.hasJump()) {
			jumpState = true;
			System.out.println("nice jump!");
		}
				
		cells.add(nextCell);
		nextCell.placeSnake(); //after all the checks if power ups exist, we now are able to change cells snake
		
		//manage food power up
		if (foodCounter == 0) { 	   //the tail of the snake is only removed if no food was eaten
			cells.getFirst().getOut(); //this way the snake gets bigger
			cells.removeFirst();
		} else {
			foodCounter--;
		}
		if (foodCounter < 0) foodCounter = 0;


		//manage boost power up
		if (boostState) {
				boostCounter++;
				if (boostCounter > Board.BOOST_SIZE)
					boostState = false;
				move(true);
		}
		
	}
				
	private boolean hasWon() {
		if (finalPosition.equals(Direction.RIGHT) && 
			cells.getLast().getX() >= board.getNumberOfCellsX() - 1)
				return true;
		if (finalPosition.equals(Direction.LEFT) && 
			cells.getLast().getX() <= 0)
				return true;
		if (finalPosition.equals(Direction.DOWN) &&
			cells.getLast().getY() <= 0)
				return true;
		if (finalPosition.equals(Direction.UP) &&
			cells.getLast().getY() >= board.getNumberOfCellsY() - 1)
				return true;
		return false;
	}

	public void run() {
		while (inGame()) {
			try { Thread.sleep(Board.SNAKE_SLEEPTIME); } catch (InterruptedException e) {}
			try {
				move();
			} catch (InvalidLocationException e) {
				e.printStackTrace();
				System.out.println("Snake " + key + " has won");
                interrupt();
//                board.gameOver(this); //calls method to end game, and passes the snake that won
                break;
			} catch (StuckException e2) {
				e2.printStackTrace();
				System.out.println("Snake " + key + " is stuck forever");
				break;
			}
			setUpdated(true);
		}
		setUpdated(false); //disables updating the UI because the snake it's not moving any more
	}
	
	@Override
	public void interrupt() {
		inGame = false;
		super.interrupt();
	}
	
	public boolean toUpdate() {
		return pushData;
	}
		
	public void setUpdated(boolean flag) { //marks snake not yet ready to update data
		pushData = flag;
		if (pushData) board.notifyUpdated();
	}
		
	public String getKey() {
		return key;
	}
			
	public boolean inGame() {
		return (!isInterrupted() && inGame);
	}
	
	public boolean gameOver() {
		return (isInterrupted() && !inGame);
	}
	
	public LinkedList<Cell> getCells() {
		return cells;
	}

	public void setCells(LinkedList<Cell> cells) {
		this.cells = cells;
	}
	
	public Direction getFinalPosition() {
		return finalPosition;
	}

    public int getSize() {
        return size;
    }
    
	public Direction getDirection() {
		return direction;
	}
	
	public void setDirection(Direction direction) {
		this.direction = direction;
		System.out.println("snake has now new direction" + direction);
	}
	
	public Cell getDestiny() {
		return destiny;
	}

	public void setDestiny(Cell destiny) {
		this.destiny = destiny;
	}
	
	@Override
	public String toString() {
		return key;
	}
    
    
}
