package gm;

public class InvalidDirection extends IllegalArgumentException {

	public InvalidDirection() {
		super("Unable to create a diagonal direction");
	}	
	
}
