package gm;

import java.util.Random;

public enum Direction {

	UP, DOWN, LEFT, RIGHT; 

	public static Direction getBackwardDirection(Direction direction) {
		if (direction.equals(DOWN)) return UP;
		if (direction.equals(UP)) return DOWN;
		if (direction.equals(RIGHT)) return LEFT;
		if (direction.equals(LEFT)) return RIGHT;
		else throw new IllegalArgumentException();
	}
	
	public static Direction getRandomDirection() {
		int r = new Random().nextInt(4) + 1;
		if (r == 1) return UP;
		if (r == 2) return DOWN;
		if (r == 3) return LEFT;
		if (r == 4) return RIGHT;
		return null;
	}
	
	public static UnitVector getDirection(Direction direction) {
		if (direction.equals(DOWN))
			return new UnitVector(0, -1);
		if (direction.equals(UP))
			return new UnitVector(0, 1);
		if (direction.equals(LEFT))
			return new UnitVector(-1, 0);
		if (direction.equals(RIGHT))
			return new UnitVector(1, 0);
		else throw new IllegalArgumentException();
	}
	
	public static Direction getDirection(UnitVector vector) {
		int x = vector.getX();
		int y = vector.getY();
		if (x == 0 && y == -1)
			return DOWN;
		if (x == 0 && y == 1)
			return UP;
		if (x == -1 && y == 0)
			return LEFT;
		if (x == 1 && y == 0)
			return RIGHT;
		else throw new InvalidDirection();
	}
	
	public static Direction getDirection(int i) {
		if (i == 0) return RIGHT;
		if (i == 1) return LEFT;
		if (i == 2) return UP;
		if (i == 3) return DOWN;
		throw new IllegalArgumentException();
	}

	public static Direction generateDirection(Cell origin, Cell destiny) throws InvalidDirection {
		//based where the user sends the snake, this will return 
		Cell vector = new Cell((destiny.getX() - origin.getX()), (destiny.getY() - origin.getY()));
		UnitVector normalized = vector.normalize();
		System.out.println("has made a normalized: " + normalized);
		Direction dir = null;
		try {
			dir = getDirection(normalized);
		} catch(IllegalArgumentException e) { };
		if (dir == null) {
			normalized = regulate(normalized);
			dir = getDirection(normalized);
		}
		return dir;
	}
	
	public static UnitVector regulate(UnitVector vector) {
		//TODO: this logic must be on teh unit vector constructor
		//if the vector does does not represent a regular direction, UP DOWN, etc, the user must have
		//selected a destiny that makes a diagonal with the snake. When this happens we make a random aproximation
		//in such a way that the snake will keep heading the set destiny, in a legal direction
		int r = new Random().nextInt(2);
		if (vector.getX() != 0 && vector.getY() != 0) {
			if (r == 0) vector.setX(0);
			if (r == 1) vector.setY(0);
		}
		return vector;
	}
			
}