import java.awt.HeadlessException;
import java.io.IOException;
import java.util.Arrays;

import gm.Board;
import gui.Window;

public class Main {

	public static void main(String[] args) {

		Board board =  new Board(20, Arrays.asList("1", "2", "3"));
		Thread game = new Thread(board);
		try {
			new Window("Snake game", 700, board).init();
			game.start();
		} catch (IOException e) {
			System.out.println("Game assets not found. Unable to start game.");
		} catch(HeadlessException e) {
			System.out.println("Client needs needs display server to run.");
		}
		
    }

}
