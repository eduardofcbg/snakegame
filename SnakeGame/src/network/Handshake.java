package network;

import java.io.Serializable;
import java.util.List;

//object that the clients send to establish the connection
public class Handshake implements NetworkData, Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<String> keys;
	private boolean join;
	
	public Handshake(Client client, boolean join) {
		if (client.getKeys().size() > 3) throw new IllegalArgumentException();
		this.keys = client.getKeys();
		this.join = join;
	}
	
	public boolean isJoining() {
		return join;
	}

	@Override
	public List<String> getKeys() {
		return keys;
	}

	@Override
	public List<Object> getContent() {
		return null;
	}

	
	
}
