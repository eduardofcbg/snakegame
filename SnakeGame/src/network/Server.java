package network;

import gm.Board;
import gm.Cell;
import gm.Snake;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

public class Server {

	public static final int PORT = 8888;
	private Pool poll;
	public long time;
	
	private List<Board> boards;
	private List<Thread> dealers;
	
	//server socket
    private static ServerSocket serverSocket;
    //client socket
    private static Socket clientSocket;
    
	private ObjectInputStream in;
	private ObjectOutputStream out;

	
	public Server() throws IOException, ClassNotFoundException {
		
		this.time = System.currentTimeMillis();	
		this.poll = new Pool(this);
		
		startServing();
		
		while(true) {
			clientSocket = serverSocket.accept(); //<-- blocks here until a client connects
	        System.out.println("Server connected to socket" + clientSocket);
	        spawnThread(clientSocket);
		}
//			handleConnection(); //blocks here while no client connects
//			handleRequest(); //when client connects, the game will start
//			handleData();
//		}
		
	}
	
	
	private void spawnThread(Socket clientSocket2) {
		
	}


	public void startServing() throws IOException, ClassNotFoundException {
		in = new ObjectInputStream(clientSocket.getInputStream());
//		out = new ObjectOutputStream(clientSocket.getOutputStream());
        System.out.println("Client connected to server socket " + clientSocket);
        System.out.println(in.readObject());
	}
	
	private void handleConnection() throws IOException {
        DealWithClient dealer = new DealWithClient();
        dealer.start();
        dealers.add(dealer);
	}
	
	private void handleData() {
		try {
			System.out.println(in.readObject());
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void handleRequest() throws UnknownHostException, IOException {
		//TODO
		NetworkData request = null; //get from client
		if (request.getClass().equals(Handshake.class)) {
			if (((Handshake) request).isJoining())
				poll.join(new Client(request.getKeys()));
			else spawnGame(request.getKeys());
		}
		if (request.getClass().equals(Command.class)) {
			Board board = mapBoard(request.getKeys().get(0));
			Cell selectedCell = (Cell) request.getContent().get(0);
			Snake selectedSnake = board.getSnake(request.getKeys().get(0));
			board.setSelected(selectedCell, selectedSnake);
		}
	}
	
	public List<Cell> sendData(Board board) {
		return board.getCells();
	}
	
	public void spawnGame(List<String> keys) {
		Board board = new Board(20, keys);
		boards.add(board);
	}
	
	private Board mapBoard(String key) {
		for(Board board : boards) {
			if (board.getKeys().contains(key)) return board; 
		}
		return null;
	}
	
	

}
