package network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

import gm.Board;
import gm.Cell;

//a seperate thread for each client
public class DealWithClient extends Thread {

	private Board board;
	
	private Socket clientSocket;
	private ObjectInputStream in;
	private ObjectOutputStream out;
		
	public DealWithClient() throws IOException {
		System.out.println("Laching dealer!");
		InetAddress adress = InetAddress.getByName(null);
		System.out.println("Mapped to adress " + adress);
		clientSocket = new Socket(adress, Server.PORT);
		System.out.println("Mapped to socket " + clientSocket);
		
		this.in = new ObjectInputStream(clientSocket.getInputStream());
		this.out = new ObjectOutputStream(clientSocket.getOutputStream());
	}
	
	@Override
	public void run() {
		while(true) {
	        try {
	        	System.out.println("staring dealer");
				out.writeObject(new Command("22", new Cell(10, 11)));
				System.out.println("Writin object to socket");
				Thread.sleep(1000);
			} catch (IOException | InterruptedException e) { e.printStackTrace(); }	
		}
	}
	
	public void makeConnection() throws IOException {
	}
	
	public void updateBoard() {
		
	}
	
	public void terminateConnecion() {
		
	}
	
}
