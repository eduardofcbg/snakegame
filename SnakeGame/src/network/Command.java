package network;

import gm.Cell;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Command implements NetworkData, Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<String> keys;
	private List<Object> cells;
	
	public Command(String key, Cell cell) {
		this.cells = Arrays.asList(cell);
		this.keys = Arrays.asList(key);
	}
	
	@Override
	public List<Object> getContent() {
		return cells;
	}

	@Override
	public List<String> getKeys() {
		return keys;
	}

}
