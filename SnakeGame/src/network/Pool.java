package network;

import gm.Board;

import java.util.LinkedList;
import java.util.List;

public class Pool extends Thread {
	
	private LinkedList<Client> clients;
	private Server server;
	
	public Pool(Server server) {
		this.clients = new LinkedList<>();
	}
	
	public void join(Client client) {
		clients.add(client);
		notify();
	}
	
	private void remove() {
		clients.removeLast();
	}
	
	public void run() {
		while(true) {
			while(!enoughPlayers())
				try { wait(); } catch (InterruptedException e) { }
			server.spawnGame(getNextKeys(getNextPlayers()));
		}
	}

	private boolean enoughPlayers() {
		if (clients.size() > Board.MINIMUM_PLAYERS) return true;
		return false;
	}
	
	private List<Client> getNextPlayers() {
		List<Client> toPlay = new LinkedList<>();
		int keys = 0;
		for (int i = 0; i < Board.MAXIMUM_PLAYERS; i++) {
			keys += clients.getLast().getKeys().size();
			if (keys > 4) break;
			toPlay.add(clients.getLast());
			remove(); //remove player from poll
		}
		return toPlay;
	}
	
	private List<String> getNextKeys(List<Client> clients) {
		List<String> keys = new LinkedList<>();
		for(Client client : clients)
			keys.addAll(client.getKeys());
		return keys;
	}
	
}
