package network;

import java.util.List;

public interface NetworkData {
	
	public List<String> getKeys();
	public List<Object> getContent();
	
}
