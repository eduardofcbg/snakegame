package network;

import gm.Cell;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

public class Client {

	private List<String> keys;
	
    private static Socket clientSocket;
    
	private ObjectInputStream in;
	private ObjectOutputStream out;
	
	public Client(List<String> keys) throws UnknownHostException, IOException {
		if (keys.size() > 4) throw new IllegalArgumentException();
		this.keys = keys;
		connect();
	}
	
	public List<String> getKeys() {
		return keys;
	}
	
	private void connect() throws UnknownHostException, IOException {
//		while(true) {
			clientSocket = new Socket("localhost", 8888);
	        System.out.println("Client connected to socket" + clientSocket);
//			in = new ObjectInputStream(clientSocket.getInputStream());
			out = new ObjectOutputStream(clientSocket.getOutputStream());
	        System.out.println("Client connected to server socket " + clientSocket);
            out.writeObject(new Command("ola", new Cell(1, 55)));
            System.out.println("Client wrote object!");
//		}
		
	}
	
	public static void main(String[] args) {
		try {
			new Client(Arrays.asList("1"));
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
